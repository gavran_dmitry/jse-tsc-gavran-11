package ru.tsc.gavran.tm.api.controller;

import ru.tsc.gavran.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void showFindTask(Task task);

    void clearTasks();

    void createTask();

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void removeById();

    void removeByName();

    void removeByIndex();

}