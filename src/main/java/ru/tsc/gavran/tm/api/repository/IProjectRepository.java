package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}